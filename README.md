# README #

Este projeto é um estudo que apresenta a implementação de um CRUD 
utilizando gRPC em Java e MongoDB.

* [Introdução](#markdown-header-introducao)
* [Passos para Execução](#markdown-header-passos-para-execução)
* [Links Uteis](#markdown-header-links-uteis)

# Introdução

Como em muitos sistemas RPC, o gRPC é baseado na ideia de definir
um serviço, especificando os métodos que podem ser chamados
remotamente com seus parâmetros e tipos de retorno.

No lado do servidor, o servidor implementa essa interface e executa
um servidor gRPC para lidar com as chamadas do cliente.

No lado do cliente, o cliente tem um stub que fornece os mesmos
métodos que o servidor.


### Passos para Execução ###

* Adjust Build and Run
    * Acessar o caminho abaixo:
  > Preferences > Build, Execution, Deployment > Build Tools > Gradle
    * Alterar para IntelliJ IDEA os seguintes campos:
        * Build and run using:
        * Run Tests using:
* Reload All Gradle Projects
  > Gradle > Reload All Gradle Projects
* Generate proto
    * Executar sempre que atualizar arquivos *.proto
  > Gradle > grpc-java > other > generateProto
* Executar servidor
  > com.cotrena.grpc.server > GreetingServer
* Executar cliente
  > com.cotrena.grpc.client > GreetingClient

### Links Uteis ###

* [Quickstart](https://grpc.io/docs/languages/java/quickstart/)
* [Documentation](https://grpc.io/docs/)
* [Error Code](https://grpc.io/docs/guides/error/)
* [Deadline](https://grpc.io/blog/deadlines/)