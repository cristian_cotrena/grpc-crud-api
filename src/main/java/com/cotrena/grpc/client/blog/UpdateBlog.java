package com.cotrena.grpc.client.blog;

import com.cotrena.grpc.base.ClientBase;
import com.proto.blog.Blog;
import com.proto.blog.BlogServiceGrpc;
import com.proto.blog.UpdateBlogRequest;
import com.proto.blog.UpdateBlogResponse;

public class UpdateBlog extends ClientBase {
    public boolean execute() {
        BlogServiceGrpc.BlogServiceBlockingStub client = BlogServiceGrpc.newBlockingStub(MANAGED_CHANNEL);

        Blog blog = Blog.newBuilder()
                .setId("625a352479cd6879a20aeddf")
                .setAuthorId("UPDATE_id_author")
                .setContent("UPDATE_content")
                .setTitle("UPDATE_title")
                .build();

        UpdateBlogRequest request = UpdateBlogRequest.newBuilder()
                .setBlog(blog)
                .build();
        try {
            UpdateBlogResponse response = client.update(request);
            return response.getUpdated();
        } catch (Exception e){
            System.out.println("Update failed:");
            e.printStackTrace();
            return false;
        }
    }
}
