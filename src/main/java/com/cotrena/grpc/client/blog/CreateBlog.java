package com.cotrena.grpc.client.blog;

import com.cotrena.grpc.base.ClientBase;
import com.proto.blog.Blog;
import com.proto.blog.BlogServiceGrpc;
import com.proto.blog.CreateBlogRequest;
import com.proto.blog.CreateBlogResponse;

public class CreateBlog extends ClientBase {
    public String execute() {
        BlogServiceGrpc.BlogServiceBlockingStub client = BlogServiceGrpc.newBlockingStub(MANAGED_CHANNEL);

        Blog blog = Blog.newBuilder()
                .setAuthorId("id_author")
                .setContent("content")
                .setTitle("title")
                .build();
        CreateBlogRequest request = CreateBlogRequest.newBuilder()
                .setBlog(blog)
                .build();

        CreateBlogResponse response = client.create(request);
        return response.getId();
    }
}
