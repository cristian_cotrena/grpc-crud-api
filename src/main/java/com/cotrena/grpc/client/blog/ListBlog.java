package com.cotrena.grpc.client.blog;

import com.cotrena.grpc.base.ClientBase;
import com.proto.blog.Blog;
import com.proto.blog.BlogServiceGrpc;
import com.proto.blog.ListBlogRequest;
import com.proto.blog.ListBlogResponse;

import java.util.List;

public class ListBlog extends ClientBase {
    public List<Blog> execute() {
        BlogServiceGrpc.BlogServiceBlockingStub client = BlogServiceGrpc.newBlockingStub(MANAGED_CHANNEL);

        ListBlogRequest request = ListBlogRequest.newBuilder().build();
        ListBlogResponse response = client.list(request);
        return response.getBlogList();
    }
}
