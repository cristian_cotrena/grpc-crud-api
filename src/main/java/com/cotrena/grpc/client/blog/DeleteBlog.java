package com.cotrena.grpc.client.blog;

import com.cotrena.grpc.base.ClientBase;
import com.proto.blog.BlogServiceGrpc;
import com.proto.blog.DeleteBlogRequest;
import com.proto.blog.DeleteBlogResponse;

public class DeleteBlog extends ClientBase {
    public boolean execute() {
        BlogServiceGrpc.BlogServiceBlockingStub client = BlogServiceGrpc.newBlockingStub(MANAGED_CHANNEL);

        DeleteBlogRequest request = DeleteBlogRequest.newBuilder()
                .setId("625a3b06c00cf3611c7b86b6")
                .build();
        try {
            DeleteBlogResponse response = client.delete(request);
            return response.getDeleted();
        } catch (Exception e){
            System.out.println("Delete failed:");
            e.printStackTrace();
            return false;
        }
    }
}
