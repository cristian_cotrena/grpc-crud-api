package com.cotrena.grpc.client.blog;

import com.cotrena.grpc.base.ClientBase;
import com.proto.blog.Blog;
import com.proto.blog.BlogServiceGrpc;
import com.proto.blog.ReadBlogRequest;
import com.proto.blog.ReadBlogResponse;

public class ReadBlog  extends ClientBase {
    public Blog execute() {
        BlogServiceGrpc.BlogServiceBlockingStub client = BlogServiceGrpc.newBlockingStub(MANAGED_CHANNEL);

       ReadBlogRequest request = ReadBlogRequest.newBuilder()
               .setId("625a352479cd6879a20aeddf")
                .build();

        ReadBlogResponse response = client.read(request);
        return response.getBlog();
    }
}
