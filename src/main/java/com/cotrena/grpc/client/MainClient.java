package com.cotrena.grpc.client;

import com.cotrena.grpc.client.blog.*;
import com.proto.blog.Blog;

import java.util.List;

public class MainClient {
    public static void main(String[] args) {
        System.out.println("Hello gRPC Client");

        CreateBlog createBlog = new CreateBlog();
        ReadBlog readBlog = new ReadBlog();
        UpdateBlog updateBlog = new UpdateBlog();
        ListBlog listBlog = new ListBlog();
        DeleteBlog deleteBlog = new DeleteBlog();

        String id = createBlog.execute();
        Blog blog = readBlog.execute();
        boolean updated = updateBlog.execute();
        List<Blog> list = listBlog.execute();
        boolean deleted = deleteBlog.execute();

        System.out.println("CREATE BLOG:");
        System.out.println("ID = " + id);

        System.out.println("READ BLOG:");
        System.out.println("id:" + blog.getId());
        System.out.println("author id:" + blog.getAuthorId());
        System.out.println("content:" + blog.getContent());
        System.out.println("title:" + blog.getTitle());

        System.out.println("UPDATE BLOG:");
        System.out.println("Updated: " + updated);

        System.out.println("LIST BLOG:");
        System.out.println("List size: " + list.size());

        System.out.println("DELETE BLOG:");
        System.out.println("Deleted: " + deleted);
    }
}
