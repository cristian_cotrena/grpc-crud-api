package com.cotrena.grpc.server;

import com.cotrena.grpc.server.blog.BlogServiceImpl;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class MainServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("Hello gRPC Server");

        Server server = ServerBuilder.forPort(50051)
                .addService(new BlogServiceImpl())
                .build();

        server.start();
        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            System.out.println("Receive Shutdown Request");
            server.shutdown();
            System.out.println("Successfully stopped the server");
        }));
        server.awaitTermination();
    }
}
