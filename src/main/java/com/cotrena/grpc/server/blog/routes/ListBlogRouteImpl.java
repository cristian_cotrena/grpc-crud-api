package com.cotrena.grpc.server.blog.routes;

import com.mongodb.client.MongoCollection;
import com.proto.blog.Blog;
import com.proto.blog.ListBlogRequest;
import com.proto.blog.ListBlogResponse;
import io.grpc.stub.StreamObserver;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class ListBlogRouteImpl {
    public void execute(MongoCollection<Document> collection,
                        ListBlogRequest request,
                        StreamObserver<ListBlogResponse> responseObserver) {

        List<Blog> allBlogs = new ArrayList<>();
        for (Document item : collection.find()) {
            allBlogs.add(Blog.newBuilder()
                    .setId(item.getObjectId("_id").toString())
                    .setAuthorId(item.getString("author_id"))
                    .setTitle(item.getString("title"))
                    .setContent(item.getString("content"))
                    .build());
        }

        ListBlogResponse response = ListBlogResponse.newBuilder()
                .addAllBlog(allBlogs)
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
