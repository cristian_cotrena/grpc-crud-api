package com.cotrena.grpc.server.blog.routes;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.proto.blog.Blog;
import com.proto.blog.ReadBlogRequest;
import com.proto.blog.ReadBlogResponse;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.bson.Document;
import org.bson.types.ObjectId;

public class ReadBlogRouteImpl {
    public void execute(MongoCollection<Document> collection,
                        ReadBlogRequest request,
                        StreamObserver<ReadBlogResponse> responseObserver) {

        Document result = collection.find(Filters.eq("_id", new ObjectId(request.getId()))).first();

        if(result == null) {
            responseObserver.onError(Status.NOT_FOUND.withDescription("Blog not found.").asRuntimeException());
            responseObserver.onCompleted();
            return;
        }

        Blog blog = Blog.newBuilder()
                .setId(request.getId())
                .setAuthorId(result.getString("author_id"))
                .setTitle(result.getString("title"))
                .setContent(result.getString("content"))
                .build();

        ReadBlogResponse response = ReadBlogResponse.newBuilder()
                .setBlog(blog)
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
