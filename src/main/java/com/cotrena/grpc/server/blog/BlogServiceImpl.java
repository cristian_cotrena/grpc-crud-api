package com.cotrena.grpc.server.blog;

import com.cotrena.grpc.server.blog.routes.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.proto.blog.*;
import io.grpc.stub.StreamObserver;
import org.bson.Document;

public class BlogServiceImpl extends BlogServiceGrpc.BlogServiceImplBase {

    private MongoClient mongoClient = MongoClients.create("mongodb://localhost:27017");
    private MongoDatabase database  = mongoClient.getDatabase("mydb");
    private MongoCollection<Document> collection = database.getCollection("blog");

    @Override
    public void create(CreateBlogRequest request, StreamObserver<CreateBlogResponse> responseObserver) {
        CreateBlogRouteImpl createService = new CreateBlogRouteImpl();
        createService.execute(collection, request, responseObserver);
    }

    @Override
    public void read(ReadBlogRequest request, StreamObserver<ReadBlogResponse> responseObserver) {
        ReadBlogRouteImpl readService = new ReadBlogRouteImpl();
        readService.execute(collection, request, responseObserver);
    }

    @Override
    public void update(UpdateBlogRequest request, StreamObserver<UpdateBlogResponse> responseObserver) {
        UpdateBlogRouteImpl updateService = new UpdateBlogRouteImpl();
        updateService.execute(collection, request, responseObserver);
    }

    @Override
    public void list(ListBlogRequest request, StreamObserver<ListBlogResponse> responseObserver) {
        ListBlogRouteImpl listService = new ListBlogRouteImpl();
        listService.execute(collection, request, responseObserver);
    }

    @Override
    public void delete(DeleteBlogRequest request, StreamObserver<DeleteBlogResponse> responseObserver) {
        DeleteBlogRouteImpl deleteService = new DeleteBlogRouteImpl();
        deleteService.execute(collection, request, responseObserver);
    }
}
