package com.cotrena.grpc.server.blog.routes;

import com.mongodb.client.MongoCollection;
import com.proto.blog.Blog;
import com.proto.blog.CreateBlogRequest;
import com.proto.blog.CreateBlogResponse;
import io.grpc.stub.StreamObserver;
import org.bson.Document;

public class CreateBlogRouteImpl {
    public void execute(MongoCollection<Document> collection,
                        CreateBlogRequest request,
                        StreamObserver<CreateBlogResponse> responseObserver) {
        Blog blog = request.getBlog();
        Document doc = new Document("author_id", blog.getAuthorId())
                .append("title", blog.getTitle())
                .append("content", blog.getContent());

        collection.insertOne(doc);

        String id = doc.getObjectId("_id").toString();

        CreateBlogResponse response = CreateBlogResponse.newBuilder()
                .setId(id)
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
