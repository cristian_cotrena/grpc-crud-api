package com.cotrena.grpc.server.blog.routes;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import com.proto.blog.DeleteBlogRequest;
import com.proto.blog.DeleteBlogResponse;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.bson.Document;
import org.bson.types.ObjectId;

public class DeleteBlogRouteImpl {
    public void execute(MongoCollection<Document> collection,
                        DeleteBlogRequest request,
                        StreamObserver<DeleteBlogResponse> responseObserver) {

        DeleteResult result = null;
        try {
            result = collection.deleteOne(Filters.eq("_id", new ObjectId(request.getId())));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result == null || !result.wasAcknowledged()) {
            responseObserver.onError(Status.INTERNAL.augmentDescription("Fail to delete").asRuntimeException());
            return;
        }

        if (result.getDeletedCount() == 0) {
            responseObserver.onError(Status.NOT_FOUND.augmentDescription("Blog not found on database").asRuntimeException());
            return;
        }

        DeleteBlogResponse response = DeleteBlogResponse.newBuilder()
                .setDeleted(true)
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
