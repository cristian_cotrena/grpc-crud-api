package com.cotrena.grpc.server.blog.routes;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.proto.blog.Blog;
import com.proto.blog.UpdateBlogRequest;
import com.proto.blog.UpdateBlogResponse;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.bson.Document;
import org.bson.types.ObjectId;

public class UpdateBlogRouteImpl {
    public void execute(MongoCollection<Document> collection,
                        UpdateBlogRequest request,
                        StreamObserver<UpdateBlogResponse> responseObserver) {
        Blog blog = request.getBlog();

        if (blog.getId().isEmpty()) {
            responseObserver.onError(Status.INVALID_ARGUMENT
                    .withDescription("id: Campo não pode ser vazio.")
                    .asRuntimeException());
            return;
        }

        Document result = collection.findOneAndUpdate(
                Filters.eq("_id", new ObjectId(blog.getId())),
                Updates.combine(
                        Updates.set("author_id",  blog.getAuthorId()),
                        Updates.set("title",  blog.getTitle()),
                        Updates.set("content",  blog.getContent())));

        if (result == null) {
            responseObserver.onError(Status.NOT_FOUND
                    .withDescription("Blog not found.")
                    .asRuntimeException());
            return;
        }

        UpdateBlogResponse response = UpdateBlogResponse.newBuilder()
                .setUpdated(true)
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
